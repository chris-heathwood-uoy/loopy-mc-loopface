require('dotenv').config();

const fs = require('fs');
const { performance } = require('perf_hooks');

const { eq, filter, get, map, pipe } = require('lodash/fp');

let students = [];

describe('loopy-mc-loopface', () => {
    beforeAll(() => {
        students = JSON.parse(fs.readFileSync('data/students.json'));
    });

    it('load all of the students', () => {
        expect(students.length).toEqual(
            parseInt(process.env.NUMBER_OF_STUDENTS, 10)
        );
    });

    it("should time how long functional programming style functions take to get all girls' countries", () => {
        const t0 = performance.now();

        const isFemale = pipe(get('gender'), eq('Girl'));
        const getCountries = map(get('country'));
        const getStudentCountries = pipe(filter(isFemale), getCountries);

        const countries = getStudentCountries(students);

        const t1 = performance.now();

        console.log(
            `Using functional programming took ${t1 - t0} milliseconds and found ${
                countries.length
            } countries.`
        );
    });

    it("should time how long new array functions take to get all girls' countries", () => {
        const t0 = performance.now();

        const countries = students
            .filter(student => student.gender === 'Girl')
            .map(student => student.country);

        const t1 = performance.now();

        console.log(
            `Using filter -> map took ${t1 - t0} milliseconds and found ${
                countries.length
            } countries.`
        );
    });

    it('should time how long a for each loop takes', () => {
        const countries = [];

        const t0 = performance.now();

        students.forEach(student => {
            if (student.gender === 'Girl') {
                countries.push(student.country);
            }
        });

        const t1 = performance.now();

        console.log(
            `Using foreach took ${t1 - t0} milliseconds and found ${
                countries.length
            } countries.`
        );
    });

    it("should time how long an old school loop takes to get all girls' countries", () => {
        const countries = [];

        const t0 = performance.now();

        // eslint-disable-next-line unicorn/no-for-loop
        for (let index = 0; index < students.length; index++) {
            if (students[index].gender === 'Girl') {
                countries.push(students[index].country);
            }
        }

        const t1 = performance.now();

        console.log(
            `Using old school for loop took ${t1 -
                t0} milliseconds and found ${countries.length} countries.`
        );
    });
});
