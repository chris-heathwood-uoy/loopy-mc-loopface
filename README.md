# Loopy McLoopface

## Getting up and running

-   `npm install`
-   `npm run setup`\*

\*The setup can take a while and generates roughly 350M of json data.

I just use nvm to switch betweeen node versions:

-   `nvm use 10.16.3`
-   `nvm use 12.13.1`

And run this:

-   `npm run test`

## Results on node v10.16.3 (from https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html)

-   Using functional programming took `571.2924730000086` milliseconds and found 499700 countries.
-   Using filter -> map took `220.98348899977282` milliseconds and found 499700 countries.
-   Using foreach took `68.0736719998531` milliseconds and found 499700 countries.
-   Using old school for loop took `34.80402599973604` milliseconds and found 499700 countries.

## Results on node v12.13.1 (from https://docs.aws.amazon.com/lambda/latest/dg/lambda-runtimes.html)

-   Using functional programming took `436.092313000001` milliseconds and found 499700 countries.
-   Using filter -> map took `61.54731799988076` milliseconds and found 499700 countries.
-   Using foreach took `34.729814999736845` milliseconds and found 499700 countries.
-   Using old school for loop took `22.573357000015676` milliseconds and found 499700 countries.

## Conclusions

Old school for loop FTW, though I think filter -> map is fine as is the functional style in most cases, just be a bit careful going full functional if performance matters.
