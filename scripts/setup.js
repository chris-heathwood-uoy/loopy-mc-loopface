require('dotenv').config();

const fs = require('fs');

const faker = require('faker');
const { DateTime } = require('luxon');
const shell = require('shelljs');

const setup = async () => {
    // Clean and setup the data folder
    shell.rm('-rf', 'data');
    shell.mkdir('-p', 'data');

    fs.writeFileSync('data/students.json', '[');

    for (let index = 0; index < process.env.NUMBER_OF_STUDENTS; index++) {
        const student = {
            id: faker.random.number(),
            firstName: faker.name.firstName(),
            lastName: faker.name.lastName(),
            dob: faker.date.past(
                20,
                DateTime.local(2001, 0, 0, 0, 0).toISODate()
            ),
            level: faker.random.number({ min: 1, max: 4 }),
            gender: faker.random.arrayElement(['Boy', 'Girl']),
            streetName: faker.address.streetName(),
            streetAddress: faker.address.streetAddress(),
            secondaryAddress: faker.address.secondaryAddress(),
            city: faker.address.city(),
            county: faker.address.county(),
            country: faker.address.country(),
            zipCode: faker.address.zipCode(),
            latitude: faker.address.latitude(),
            longitude: faker.address.longitude(),
        };

        const login =
            student.firstName.charAt(0) +
            student.lastName.charAt(0) +
            faker.random.number();

        student.login = login.toLowerCase();

        let studentJson = '';

        if (index > 0) {
            studentJson = ',';
        }

        studentJson += JSON.stringify(student);

        fs.appendFileSync('data/students.json', studentJson);
    }

    fs.appendFileSync('data/students.json', ']');
};

setup();
